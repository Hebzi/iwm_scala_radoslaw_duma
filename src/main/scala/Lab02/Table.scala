package Lab02

class Table {


  def tab(tr: Int, td: Int, content: String = ""): Unit = {
    println("<table>")
    for (_ <- 1 to tr) {
      println("\t<tr>")
      for (_ <- 1 to td) {
        println(s"\t\t<td>$content</td>")
      }
      println("\t</tr>")
    }
    println("</table>")
  }
}
