package Lab02

class List {

  def ol(elements: Int): Unit = {
    println("<ol>")
    for (_ <- 1 to elements) {
      println("\t<li></li>")
    }
    println("</ol>")
  }

  def ul(elements: Int): Unit = {
    println("<ul>")
    for (_ <- 1 to elements) {
      println("\t<li></li>")
    }
    println("</ul>")
  }

}
