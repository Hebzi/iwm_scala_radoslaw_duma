package Lab02

class Img {

  def img(src: String): Unit = {
    println(s"""<img src="$src" alt="">""")
  }

  def img(src: String, width: String = "", height: String = ""): Unit = {
    if (width != "" && height != "") {
      println(s"""<img src="$src" alt="" width="$width" height="$height">""")
    } else if (width != "" & height == "") {
      println(s"""<img src="$src" alt="" width="$width">""")
    } else if (width == "" & height != "") {
      println(s"""<img src="$src" alt="" height="$height">""")
    }
  }

}
