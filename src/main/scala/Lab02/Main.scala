package Lab02

object Main {
  def main(args: Array[String]): Unit = {

    val h = new H()
    h.heading("This is heading 1", 1)
    h.heading("This is heading 3", 3)
    println()

    val p = new P()
    p.paragraph("This is paragraph")
    println()

    val table = new Table()
    table.tab(1, 3)
    println()

    table.tab(3, 2)
    println()

    val lst = new List()
    lst.ol(3)
    println()
    lst.ul(4)
    println()

    val img = new Img()
    img.img("pic1.jpg")
    img.img("pic2.jpg", width = "100px")
    img.img("pic3.jpg", height = "500px")
    img.img("pic4.jpg", width = "200", height = "150px")

  }
}
