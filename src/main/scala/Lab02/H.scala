package Lab02

class H {

  def heading(h: String, num: Int) {
    if (1 <= num && num <= 6) {
      println(s"<h$num>$h</h$num>")
    }
  }
}
