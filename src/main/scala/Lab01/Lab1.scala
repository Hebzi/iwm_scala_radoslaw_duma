package Lab01

import scala.collection.mutable.ListBuffer

object Lab1 {
  def main(args: Array[String]): Unit = {

    println("---|Zadanie 1|---")
    starSquare(8, 3)

    println("\n\n---|Zadanie 2|---")
    calculateZad2()

    println("\n\n---|Zadanie 3|---")
    findingCubeEquality()
  }


  def starSquare(x: Int, y: Int): Unit = {
    for (_ <- 1 to y) {
      for (_ <- 1 to x) {
        print("*")
      }
      println()
    }
  }


  def calculateZad2(): Unit = {
    val w = math.cbrt(2 * math.log10(521) + (7.5e8 / (128 * 422)))
    println("w = " + w)
  }


  def findingCubeEquality(): Unit = {
    var goodResults = new ListBuffer[Int]()

    for (i <- 100 to 999) {
      val xList: List[Char] = i.toString.toList

      var sum: Int = 0
      for (j <- xList) {
        val result: Int = j.asDigit * j.asDigit * j.asDigit
        sum += result
      }

      if (sum == i) {
        goodResults += i
      }
    }

    println(goodResults)
  }

}
